package Classe;

import Classe.Raid;

import java.util.Date;

public class equipe {
	private int numero, temps_global, classement, penalites_bonif;
	private String nom;
	private Date date_inscription;
	private Raid unRaid;
	
	public Raid getUnRaid() {
		return unRaid;
	}
	public void setUnRaid(Raid unRaid) {
		this.unRaid = unRaid;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getTemps_global() {
		return temps_global;
	}
	public void setTemps_global(int temps_global) {
		this.temps_global = temps_global;
	}
	public int getClassement() {
		return classement;
	}
	public void setClassement(int classement) {
		this.classement = classement;
	}
	public int getPenalites_bonif() {
		return penalites_bonif;
	}
	public void setPenalites_bonif(int penalites_bonif) {
		this.penalites_bonif = penalites_bonif;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Date getDate_inscription() {
		return date_inscription;
	}
	public void setDate_inscription(Date date_inscription) {
		this.date_inscription = date_inscription;
	}

	public equipe(int numero, int temps_global, int classement, int penalites_bonif, String nom, Date date_inscription,
			Raid unRaid) {
		super();
		this.numero = numero;
		this.temps_global = temps_global;
		this.classement = classement;
		this.penalites_bonif = penalites_bonif;
		this.nom = nom;
		this.date_inscription = date_inscription;
		this.unRaid = unRaid;
	}


	
}
