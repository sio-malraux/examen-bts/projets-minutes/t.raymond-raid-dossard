package Classe;

import Classe.Raid;
import Classe.equipe;
import Classe.coureur;

public class integrer_equipe {
	private Raid unRaid;
	private coureur unCoureur;
	private equipe uneEquipe;
	private int temps_individuel;
	private String num_dossard;
	
	public int getTemps_individuel() {
		return temps_individuel;
	}
	public void setTemps_individuel(int temps_individuel) {
		this.temps_individuel = temps_individuel;
	}
	public String getNum_dossard() {
		return num_dossard;
	}
	public void setNum_dossard(String num_dossard) {
		this.num_dossard = num_dossard;
	}
	public Raid getUnRaid() {
		return unRaid;
	}
	public void setUnRaid(Raid unRaid) {
		this.unRaid = unRaid;
	}
	public coureur getUnCoureur() {
		return unCoureur;
	}
	public void setUnCoureur(coureur unCoureur) {
		this.unCoureur = unCoureur;
	}
	public equipe getUneEquipe() {
		return uneEquipe;
	}
	public void setUneEquipe(equipe uneEquipe) {
		this.uneEquipe = uneEquipe;
	}
	public integrer_equipe(Raid unRaid, coureur unCoureur, equipe uneEquipe, int temps_individuel, String num_dossard) {
		this.unRaid = unRaid;
		this.unCoureur = unCoureur;
		this.uneEquipe = uneEquipe;
		this.temps_individuel = temps_individuel;
		this.num_dossard = num_dossard;
	}
	

}
