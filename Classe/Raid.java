package Classe;

public class Raid {
	private int nb_femme, duree_max, age_minimum, montant_inscription, nb_maxi_par_equipe;
	private String code, nom, date_debut, ville, region;
	public int getNb_femme() {
		return nb_femme;
	}
	public void setNb_femme(int nb_femme) {
		this.nb_femme = nb_femme;
	}
	public int getDuree_max() {
		return duree_max;
	}
	public void setDuree_max(int duree_max) {
		this.duree_max = duree_max;
	}
	public int getAge_minimum() {
		return age_minimum;
	}
	public void setAge_minimum(int age_minimum) {
		this.age_minimum = age_minimum;
	}
	public int getMontant_inscription() {
		return montant_inscription;
	}
	public void setMontant_inscription(int montant_inscription) {
		this.montant_inscription = montant_inscription;
	}
	public int getNb_maxi_par_equipe() {
		return nb_maxi_par_equipe;
	}
	public void setNb_maxi_par_equipe(int nb_maxi_par_equipe) {
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(String date_debut) {
		this.date_debut = date_debut;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Raid(int nb_femme, int duree_max, int age_minimum, int montant_inscription, int nb_maxi_par_equipe,
			String code, String nom, String date_debut, String ville, String region) {
		this.nb_femme = nb_femme;
		this.duree_max = duree_max;
		this.age_minimum = age_minimum;
		this.montant_inscription = montant_inscription;
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
		this.code = code;
		this.nom = nom;
		this.date_debut = date_debut;
		this.ville = ville;
		this.region = region;
	}
	public Raid() {
		this.nb_femme = 2;
		this.duree_max = 1;
		this.age_minimum = 3;
		this.montant_inscription = 4;
		this.nb_maxi_par_equipe = 5;
		this.code = "";
		this.nom = "";
		this.date_debut = "";
		this.ville = "";
		this.region = "";
	}
	public Raid(String code, String nom, int nb_femme, int nb_maxi_par_equipe) {
		this.nb_femme = nb_femme;
		this.nb_maxi_par_equipe = nb_maxi_par_equipe;
		this.code = code;
		this.nom = nom;
	}
	


}
