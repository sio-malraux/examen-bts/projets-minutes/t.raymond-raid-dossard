package Classe;
import Classe.activite;
import Classe.Raid;

public class activite_raid {
	private activite uneActivite;
	private Raid unRaid;
	public activite getUneActivite() {
		return uneActivite;
	}


	public void setUneActivite(activite uneActivite) {
		this.uneActivite = uneActivite;
	}


	public Raid getUnRaid() {
		return unRaid;
	}


	public void setUnRaid(Raid unRaid) {
		this.unRaid = unRaid;
	}


	public activite_raid(activite uneActivite, Raid unRaid) {
		super();
		this.uneActivite = uneActivite;
		this.unRaid = unRaid;
	}
	
	
}
