package Classe;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class coureur {
	private String licence, nom, prenom, sexe, nationalite, mail;
	private boolean certif_med_aptitude;
	private  Date date_naissance;
	private int num_equipe, ordre_equipe;
	
	public int getNum_equipe() {
		return num_equipe;
	}
	public void setNum_equipe(int num_equipe) {
		this.num_equipe = num_equipe;
	}
	public int getOrdre_equipe() {
		return ordre_equipe;
	}
	public void setOrdre_equipe(int ordre_equipe) {
		this.ordre_equipe = ordre_equipe;
	}
	public String getLicence() {
		return licence;
	}
	public void setLicence(String licence) {
		this.licence = licence;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public boolean isCertif_med_aptitude() {
		return certif_med_aptitude;
	}
	public void setCertif_med_aptitude(boolean certif_med_aptitude) {
		this.certif_med_aptitude = certif_med_aptitude;
	}
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	public coureur(String licence, String nom, String prenom, String sexe, String nationalite, String mail,
			boolean certif_med_aptitude, Date date_naissance) {
		this.licence = licence;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.nationalite = nationalite;
		this.mail = mail;
		this.certif_med_aptitude = certif_med_aptitude;
		this.date_naissance = date_naissance;
	}

	public coureur() {
		this.licence = "test";
		this.nom = "thomas";
		this.prenom = "thomas";
		this.sexe = "homme";
		this.nationalite = "français";
		this.mail = "test@test.com";
		this.certif_med_aptitude = true;
		this.date_naissance = new Date();
	}
	public coureur(String licence, String nom, String prenom, int num_equipe, int ordre_equipe) {
		this.licence = licence;
		this.nom = nom;
		this.prenom = prenom;
		this.num_equipe = num_equipe;
		this.ordre_equipe = ordre_equipe;
	}
	
	public void generer_dossard(String code_raid, Statement stmt) {
		//String num_dossard = this.num_equipe + code_raid + this.prenom.substring(0,1) + this.nom.substring(0,1) + this.num_equipe;
		String num_dossard = this.num_equipe + "-" + code_raid + "-" + this.prenom.substring(0,1).toUpperCase() + this.nom.substring(0,1).toUpperCase() + "-" + this.ordre_equipe; 
		try {
			stmt.executeQuery("insert into raid_dossard.integrer_equipe (licence, numero_equipe, code_raid, num_dossard) values (" + this.licence + ", " + this.num_equipe + ", " + code_raid + ", " + num_dossard + ")");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
