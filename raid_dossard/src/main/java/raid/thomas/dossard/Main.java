package raid.thomas.dossard;

import raid.thomas.dossard.classes.Raid;
import raid.thomas.dossard.classes.Coureur;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Coureur coureur = new Coureur("Licence test","nom","Prenomtest",5,5);
		System.out.println(coureur.toString());
		
		String url = "jdbc:postgresql://localhost:1999/t.raymond"; //"jdbc:postgresql://postgresql.bts-malraux72.net:5432/t.raymond";
		Connection con;
		try {
			//Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(url, "t.raymond", "P@ssword");
			Statement stmt = con.createStatement();
			
			ResultSet rs;
			String code = "RAID2";
			rs = stmt.executeQuery("Select * from raid_dossard.raid where code = '" + code + "'");
			if(rs.next() == false) {
				System.out.println("It's empty !, it dosn't exist !");
			}else {
				Raid leRaid = new Raid(rs.getString("code"), rs.getString("nom"), rs.getInt("nb_maxi_par_equipe"), rs.getInt("nb_femmes"));
				System.out.println(leRaid.toString());

				//Vérification de l'existence
				
				rs = stmt.executeQuery("Select count(*) from raid_dossard.integrer_equipe where num_dossard is not null AND code_raid = '" + leRaid.getCode() + "'");
				int tester=0;
				
				while(rs.next()) {tester = rs.getInt(1);}
				
				if(tester == 0) {
					System.out.println("It dosn't made it !");
				}else {
					System.out.println("World");

					rs = stmt.executeQuery(
							"select C.licence,--Renvoie tout les coureurs appartenant à une équipe valide + leur numero_dans_equipe attribué\r\n" + 
							"	nom,\r\n" + 
							"	prenom,\r\n" + 
							"	numero_equipe,\r\n" + 
							"	ROW_NUMBER() OVER(PARTITION BY numero_equipe) AS numero_dans_equipe\r\n" + 
							"	from raid_dossard.integrer_equipe I\r\n" + 
							"inner join raid_dossard.coureur C ON C.licence = I.licence\r\n" + 
							"WHERE numero_equipe IN (\r\n" + 
							"--Equipes valides--\r\n" + 
							"select numero_equipe from raid_dossard.integrer_equipe I\r\n" + 
							"inner join raid_dossard.coureur C ON C.licence = I.licence\r\n" + 
							"WHERE code_raid = '" + leRaid.getCode() + "'--code de l'objet RAID\r\n" + 
							"group by numero_equipe\r\n" + 
							"HAVING COUNT(C.licence) = '" + leRaid.getNb_maxi_par_equipe() + "' and COUNT(C.licence)filter (where sexe = 'F') >= '" + leRaid.getNb_femmes() + "' --nb_maxi_par_equipe et nb_femmes\r\n" + 
							")\r\n" + 
							"AND code_raid ='" + leRaid.getCode() + "'");
					
					List<Coureur> lesCoureurs = new ArrayList<Coureur>();
					Coureur leCoureur;
					while(rs.next()) {
						System.out.println("Un coureur");
					leCoureur = new Coureur(rs.getString("licence"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("num_equipe"), rs.getInt("ordre_equipe"));
					lesCoureurs.add(leCoureur);
					}
					
					
					for(Coureur c : lesCoureurs) {
						c.generer_dossard(leRaid.getCode(), stmt);
						System.out.println("Une géné");
					}
				}
			}
			
			/*rs = stmt.executeQuery("SELECT * FROM raid_dossard.raid");
			while(rs.next()) {
				System.out.println(rs.getString("code"));
				System.out.println(rs.getString("nom"));
				System.out.println(rs.getInt("nb_max_par_equipe"));
				System.out.println(rs.getInt("nb_femmes"));
				}*/
		} catch (SQLException e){ //| ClassNotFoundException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
