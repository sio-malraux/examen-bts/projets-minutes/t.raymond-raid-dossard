package raid.thomas.dossard.classes; 

public class Raid { 

        private int nb_femmes, nb_maxi_par_equipe;
        
        private String code, nom;
        
        
        public int getNb_femmes() {
			return nb_femmes;
		}


		public void setNb_femmes(int nb_femmes) {
			this.nb_femmes = nb_femmes;
		}


		public int getNb_maxi_par_equipe() {
			return nb_maxi_par_equipe;
		}


		public void setNb_maxi_par_equipe(int nb_maxi_par_equipe) {
			this.nb_maxi_par_equipe = nb_maxi_par_equipe;
		}


		public String getCode() {
			return code;
		}


		public void setCode(String code) {
			this.code = code;
		}


		public String getNom() {
			return nom;
		}


		public void setNom(String nom) {
			this.nom = nom;
		}


		public Raid(String code, String nom, int nb_femme, int nb_maxi_par_equipe) { 
        	
        	this.nb_femmes = nb_femme; 
        	this.nb_maxi_par_equipe = nb_maxi_par_equipe; 
        	this.code = code; 
        	this.nom = nom;
	}

		@Override
		public String toString() {
			return "Raid [nb_femmes=" + nb_femmes + ", nb_maxi_par_equipe=" + nb_maxi_par_equipe + ", code=" + code
					+ ", nom=" + nom + "]";
		}	
}

