package raid.thomas.dossard.classes;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;


public class Coureur {

	private String licence, nom, prenom;
	private int numero_equipe;
	private long numero_dans_equipe;
	


	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNumero_equipe() {
		return numero_equipe;
	}

	public void setNumero_equipe(int numero_equipe) {
		this.numero_equipe = numero_equipe;
	}

	public long getNumero_dans_equipe() {
		return numero_dans_equipe;
	}

	public void setNumero_dans_equipe(long numero_dans_equipe) {
		this.numero_dans_equipe = numero_dans_equipe;
	}

	public Coureur(String licence, String nom, String prenom, int numero_equipe, long numero_dans_equipe) {
		this.licence = licence;
		this.nom = nom;
		this.prenom = prenom;
		this.numero_equipe = numero_equipe;
		this.numero_dans_equipe = numero_dans_equipe;
	}
	
	public void generer_dossard(String code_raid, Statement stmt) {
		String num_dossard = this.numero_equipe + "-" + code_raid + "-" + this.prenom.substring(0,1).toUpperCase() + this.nom.substring(0,1).toUpperCase() + "-" + this.numero_dans_equipe; 
		try {
			stmt.executeQuery("insert into raid_dossard.integrer_equipe (licence, numero_equipe, code_raid, num_dossard) values (" + this.licence + ", " + this.numero_equipe + ", " + code_raid + ", " + num_dossard + ")");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Coureur [licence=" + licence + ", nom=" + nom + ", prenom=" + prenom + ", numero_equipe="
				+ numero_equipe + ", numero_dans_equipe=" + numero_dans_equipe + "]";
	}

}

