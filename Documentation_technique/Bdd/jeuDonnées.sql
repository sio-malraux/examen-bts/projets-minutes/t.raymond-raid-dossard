﻿SET client_encoding = 'UTF8';
DROP SCHEMA "raid_dossard" CASCADE;;
CREATE SCHEMA "raid_dossard";
SET SCHEMA 'raid_dossard';

CREATE TABLE "raid_dossard"."activite" (
    "code" character varying(5) NOT NULL,
    "libelle" character varying(50)
);
CREATE TABLE "raid_dossard"."activite_raid" (
    "code_raid" character varying(6) NOT NULL,
    "code_activite" character varying(5) NOT NULL
);
CREATE TABLE "raid_dossard"."coureur" (
    "licence" character varying(5) NOT NULL,
    "nom" character varying(50),
    "prenom" character varying(50),
    "sexe" character(1),
    "nationalite" character varying(50),
    "mail" character varying(50),
    "certif_med_aptitude" boolean DEFAULT false,
    "date_naissance" "date"
);
CREATE TABLE "raid_dossard"."equipe" (
    "numero" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "nom" character varying(50),
    "date_inscription" "date" DEFAULT "now"(),
    "temps_global" integer DEFAULT 0,
    "classement" integer DEFAULT 0,
    "penalites_bonif" integer DEFAULT 0
);
CREATE TABLE "raid_dossard"."integrer_equipe" (
    "numero_equipe" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "licence" character varying(5) NOT NULL,
    "temps_individuel" integer DEFAULT 0,
    "num_dossard" character varying(15)
);
CREATE TABLE "raid_dossard"."raid" (
    "code" character varying(6) NOT NULL,
    "nom" character varying(50),
    "date_debut" character varying(10),
    "ville" character varying(50),
    "region" character varying(50),
    "nb_maxi_par_equipe" integer,
    "montant_inscription" numeric(8,2),
    "nb_femmes" integer,
    "duree_maxi" integer,
    "age_minimum" integer
);
ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "pk_activite_raid" PRIMARY KEY ("code_raid", "code_activite");
ALTER TABLE ONLY "raid_dossard"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");
ALTER TABLE ONLY "raid_dossard"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");
ALTER TABLE ONLY "raid_dossard"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_dossard"."activite"("code");
ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey" FOREIGN KEY ("licence") REFERENCES "raid_dossard"."coureur"("licence");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_dossard"."equipe"("numero", "code_raid");
GRANT USAGE ON SCHEMA "raid_dossard" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite_raid" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."coureur" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."integrer_equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."raid" TO "etudiants-slam";

--jeu de données

INSERT INTO activite (code, libelle) VALUES
	('VTT-1', 'VTT'),
	('TRK-1','trekking'),
	('CNG-1','canyoning'),
	('MONT1','montagne'),
	('CHV-1','cheval'),
	('ORT-1','orientation'),
	('EVS-1','eaux vives'),
	('SPE-1','spéléologie'),
	('COR-1','cordes');
	
-- Duree en jours
INSERT INTO raid (code, nom, date_debut, ville, region, nb_maxi_par_equipe, montant_inscription, nb_femmes, duree_maxi, age_minimum) VALUES
	('RAID1', 'Test Raid 1', '01-02-2020', 'Le Mans', 'Pays-de-la-Loire', 6, 140.22, 2,150 , 17),
	('RAID2','Test raid 1', '02-04-2018', 'Paris', 'Ile de France', 4, 100, 1, 160, 16),
	('RAID3', 'Test Raid 3', '04-06-2019', 'Pau', 'Nouvelle Aquitaine', 8, 140.22, 1,170 , 18);
	
	
INSERT INTO activite_raid (code_raid, code_activite) VALUES
	('RAID1','VTT-1'),
	('RAID1','TRK-1'),
	('RAID1','CNG-1'),
	('RAID1','MONT1'),
	('RAID1','CHV-1'),
	('RAID1','ORT-1'),
	('RAID1','EVS-1'),
	('RAID1','SPE-1'),
	('RAID1','COR-1'),
	('RAID2','ORT-1'),
	('RAID2','EVS-1'),
	('RAID3','SPE-1'),
	('RAID2','COR-1'),
	('RAID3','ORT-1'),
	('RAID3','EVS-1'),
	('RAID2','SPE-1'),
	('RAID3','COR-1'),
	('RAID2','TRK-1'),
	('RAID2','CNG-1');

-- temps en jours
INSERT INTO equipe (numero, code_raid, nom, date_inscription, temps_global, classement, penalites_bonif) VALUES
	(1, 'RAID1', 'Equipe 1 R1', '02-02-2020', 6, 2, 2),
	(2, 'RAID1', 'Equipe 2 R1', '16-03-2020', 5, 1, 0),
	(3, 'RAID1', 'Equipe 3 R1', '27-01-2020', 7, 3, 1),
	(4, 'RAID2', 'Equipe 1 R2', '02-02-2018', 6, 2, 2),
	(5, 'RAID2', 'Equipe 2 R2', '16-03-2018', 5, 1, 0),
	(6, 'RAID2', 'Equipe 3 R2', '27-01-2018', 7, 3, 1),
	(1, 'RAID3', 'Equipe 1 R3', '02-02-2019', 6, 2, 2),
	(2, 'RAID3', 'Equipe 2 R3', '16-03-2019', 5, 1, 0),
	(3, 'RAID3', 'Equipe 3 R3', '27-01-2019', 7, 3, 1);
	
INSERT INTO coureur (licence, nom, prenom, sexe, nationalite, mail, certif_med_aptitude, date_naissance) VALUES
	('AA-11', 'Jean', 'Charles', 'M', 'Francais','c.oui@osef.com', true, '01-06-2000'),
	('BB-11', 'Pierre', 'Didier', 'M', 'Francais', 'c.oui@osef.com', true, '01-01-2000'),
	('CC-11', 'Join', 'Michel', 'M', 'Francais', 'c.oui@osef.com', true, '01-02-2000'),
	('DD-11', 'Auguste', 'Perceuse', 'M', 'Francais', 'c.oui@osef.com', true, '09-07-2000'),
	('EE-11', 'Artifice', 'Alex', 'M', 'Francais', 'c.oui@osef.com', true, '12-08-2000'),
	('FF-11', 'coucou', 'Felice', 'F', 'Francais', 'c.oui@osef.com', true, '04-05-2000'),
	('GG-11', 'Shadow', 'Legend', 'M', 'Francais', 'c.oui@osef.com', true, '07-08-2000'),
	('HH-11', 'Quoi', 'Hehehe', 'M', 'Francais', 'c.oui@osef.com', true, '01-01-1994'),
	('II-11', 'Georges', 'Celine', 'F', 'Francais', 'c.oui@osef.com', true, '02-09-2001'),
	('JJ-11', 'Jungle', 'Albert', 'M', 'Francais', 'c.oui@osef.com', true, '13-10-2000'),
	('KK-11', 'Arthuro', 'Jean', 'M', 'Francais', 'c.oui@osef.com', true, '03-05-1999'),
	('LL-11', 'Tokyo', 'Gerard', 'M', 'Francais', 'c.oui@osef.com', true, '05-07-1991'),
	('MM-11', 'Jousse', 'Sandy', 'F', 'Francais', 'c.oui@osef.com', true, '01-01-1998'),
	('NN-11', 'Berlin', 'Wtf', 'M', 'Francais', 'c.oui@osef.com', true, '01-12-2002'),
	('OO-11', 'Charly', 'Cedric', 'M', 'Francais', 'c.oui@osef.com', true, '19-11-1975'),
	('AA-22', 'Jean2', 'Charles', 'M', 'Francais','c.oui@osef.com', true, '01-06-2000'),
	('BB-22', 'APierre', 'Didier', 'M', 'Francais', 'c.oui@osef.com', true, '01-01-2000'),
	('CC-22', 'Join2', 'Michel', 'M', 'Francais', 'c.oui@osef.com', true, '01-02-2000'),
	('DD-22', 'Auguste2', 'Perceuse', 'M', 'Francais', 'c.oui@osef.com', true, '09-07-2000'),
	('EE-22', 'DArtifice', 'Alex', 'M', 'Francais', 'c.oui@osef.com', true, '12-08-2000'),
	('FF-22', 'Peroquet2', 'Alaine', 'F', 'Francais', 'c.oui@osef.com', true, '04-05-2000'),
	('GG-22', 'Shadow2', 'Legend', 'M', 'Francais', 'c.oui@osef.com', true, '07-08-2000'),
	('HH-22', 'Quoi2', 'Hehehe', 'F', 'Francais', 'c.oui@osef.com', true, '01-01-1994'),
	('II-22', 'Georges2', 'Celine', 'F', 'Francais', 'c.oui@osef.com', true, '02-09-2001'),
	('JJ-22', 'Jungle2', 'Albert', 'M', 'Francais', 'c.oui@osef.com', true, '13-10-2000'),
	('KK-22', 'LArthuro', 'Jean', 'M', 'Francais', 'c.oui@osef.com', true, '03-05-1999'),
	('LL-22', 'Tokyo2', 'Gerard', 'M', 'Francais', 'c.oui@osef.com', true, '05-07-1991'),
	('MM-22', 'Jousse2', 'Sandy', 'F', 'Francais', 'c.oui@osef.com', true, '01-01-1998'),
	('NN-22', 'Berline', 'Wtf', 'F', 'Francais', 'c.oui@osef.com', true, '01-12-2002'),
	('OO-22', 'Charly2', 'Cedric', 'M', 'Francais', 'c.oui@osef.com', true, '19-11-1975'),
	('FFFF1', 'Essaye', 'Fouf', 'F', 'Francais', 'c.oui@osef.com', true, '01-01-1994'),
	('FFFF2', 'Georgette', 'Flavie', 'F', 'Francais', 'c.oui@osef.com', true, '06-09-2001'),
	('FFFF3', 'Jung', 'Caro', 'F', 'Francais', 'c.oui@osef.com', true, '13-10-2000'),
	('FFFF4', 'LArthur', 'Jeanette', 'F', 'Francais', 'c.oui@osef.com', true, '03-05-1999'),
	('FFFF5', 'Toky', 'Lucie', 'F', 'Francais', 'c.oui@osef.com', true, '05-07-1991'),
	('FFFF6', 'Jouse', 'Vero', 'F', 'Francais', 'c.oui@osef.com', true, '01-01-1998');

-- temps en jours
INSERT INTO integrer_equipe (numero_equipe, code_raid, licence, temps_individuel) VALUES
	(1, 'RAID1', 'AA-11', 5),
	(1, 'RAID1', 'FF-11', 4),
	(1, 'RAID1', 'II-11', 5),
	(1, 'RAID1', 'NN-11', 5),
	(1, 'RAID1', 'MM-11', 8),
	(1, 'RAID1', 'JJ-11', 9),
	(2, 'RAID1', 'FFFF1', 5),
	(2, 'RAID1', 'FFFF2', 4),
	(2, 'RAID1', 'DD-11', 5),
	(2, 'RAID1', 'EE-11', 5),
	(2, 'RAID1', 'KK-11', 8),
	(2, 'RAID1', 'LL-11', 9),
	(4, 'RAID2', 'AA-11', 5),
	(4, 'RAID2', 'FF-11', 4),
	(4, 'RAID2', 'II-11', 5),
	(4, 'RAID2', 'NN-11', 5),
	(5, 'RAID2', 'FFFF1', 5),
	(5, 'RAID2', 'FFFF2', 4),
	(5, 'RAID2', 'DD-11', 5),
	(5, 'RAID2', 'EE-11', 5),
	(6, 'RAID2', 'KK-11', 8),
	(6, 'RAID2', 'LL-11', 9),
	(6, 'RAID2', 'FF-22', 9),
	(1, 'RAID3', 'AA-11', 5),
	(1, 'RAID3', 'FF-11', 4),
	(1, 'RAID3', 'II-11', 5),
	(1, 'RAID3', 'NN-11', 5),
	(1, 'RAID3', 'MM-11', 8),
	(1, 'RAID3', 'JJ-11', 9),
	(1, 'RAID3', 'FF-22', 4),
	(1, 'RAID3', 'II-22', 5),
	(2, 'RAID3', 'FFFF1', 5),
	(2, 'RAID3', 'FFFF2', 4),
	(2, 'RAID3', 'DD-11', 5),
	(2, 'RAID3', 'EE-11', 5),
	(2, 'RAID3', 'FFFF4', 5),
	(2, 'RAID3', 'FFFF5', 4),
	(2, 'RAID3', 'DD-22', 5),
	(2, 'RAID3', 'EE-22', 5),
	(3, 'RAID3', 'KK-11', 8),
	(3, 'RAID3', 'LL-11', 9),
	(3, 'RAID3', 'FF-22', 9),
	(3, 'RAID3', 'KK-22', 8),
	(3, 'RAID3', 'LL-22', 9),
	(3, 'RAID3', 'AA-22', 9),
	(3, 'RAID3', 'BB-22', 9),
	(3, 'RAID3', 'GG-11', 9);

	